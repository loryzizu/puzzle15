const  HOW_MANY_SHUFFLE=2000;

//Elements=[pieceIndex]
var table=[[],[], [], []];

//Elements=[div, row, col]
var pieces=[[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];

window.onload=function initPage(){
    //create 15 divs
    var pieceIndex=0;
    for(var i=0; i < 4;i++){
        for(var j=0; j < 4;j++){
            if(i==3 && j==3){
                table[i][j]=null;
                break;
            }
            pieces[pieceIndex][0]=document.createElement("div");
            pieces[pieceIndex][0].innerHTML=pieceIndex;
            pieces[pieceIndex][0].setAttribute("onclick","moveAndRerender(this)");
            pieces[pieceIndex][1]=i;
            pieces[pieceIndex][2]=j;
            table[i][j]=pieceIndex;
            pieceIndex++;
        }
    }
    //shuffle
    shuffle();
    //positionate
    setPiecesPositions();
    
}

function setPiecesPositions(){
    var anchestor=document.getElementById("game");
    for(var i=0; i < 4;i++){
        for(var j=0; j < 4;j++) {
            if (table[i][j] != null) {
                pieces[table[i][j]][0].className= "puzzlePiece ";
                switch (i) {
                    case 0:
                        pieces[table[i][j]][0].className+= " firstRowPiece ";
                        break;
                    case 1:
                        pieces[table[i][j]][0].className+= " secondRowPiece ";
                        break;
                    case 2:
                        pieces[table[i][j]][0].className+= " thirdRowPiece ";
                        break;
                    case 3:
                        pieces[table[i][j]][0].className+= " fourthRowPiece ";
                        break;
                }
                switch (j) {
                    case 0:
                        pieces[table[i][j]][0].className += " firstColPiece";
                        break;
                    case 1:
                        pieces[table[i][j]][0].className += " secondColPiece";
                        break;
                    case 2:
                        pieces[table[i][j]][0].className += " thirdColPiece";
                        break;
                    case 3:
                        pieces[table[i][j]][0].className += " fourthColPiece";
                        break;
                }

                anchestor.appendChild(pieces[table[i][j]][0]);
            }
        }
    }
}

/*return true if moved false otherwise*/
function move(element){
    var pieceIndex=parseInt(element.innerHTML);
    var piece=pieces[pieceIndex];
    var pieceRow=pieces[pieceIndex][1];
    var pieceCol=pieces[pieceIndex][2];
    var moved=false;
    //can I move up?
    if(pieceRow>0 && table[pieceRow-1][pieceCol]==null){
        table[pieceRow-1][pieceCol]=pieceIndex;
        table[pieceRow][pieceCol]=null;
        pieces[pieceIndex][1]--;
        moved=true;
    }
    //can I move down?
    else if(pieceRow<3 && table[pieceRow+1][pieceCol]==null){
        table[pieceRow+1][pieceCol]=pieceIndex;
        table[pieceRow][pieceCol]=null;
        pieces[pieceIndex][1]++;
        moved=true;
    }
    //can I move left?
    else if(pieceCol>0 && table[pieceRow][pieceCol-1]==null){
        table[pieceRow][pieceCol-1]=pieceIndex;
        table[pieceRow][pieceCol]=null;
        pieces[pieceIndex][2]--;
        moved=true;
    }

    //can I move right?
    else if(pieceCol<3 && table[pieceRow][pieceCol+1]==null){
        table[pieceRow][pieceCol+1]=pieceIndex;
        table[pieceRow][pieceCol]=null;
        pieces[pieceIndex][2]++;
        moved=true;
    }

    return moved;
}

/*Called on piece click*/
function moveAndRerender(element){
    move(element);
    //it costs too much...but I'm lazy
    setPiecesPositions();
}

function shuffle(){
    for(var i=0; i< HOW_MANY_SHUFFLE; i++){
        var randomPieceIndex= Math.floor(Math.random() * 15);
        var pieceDivElement=pieces[randomPieceIndex][0];
        if(!move(pieceDivElement)){
            i--;
        }
    }
}